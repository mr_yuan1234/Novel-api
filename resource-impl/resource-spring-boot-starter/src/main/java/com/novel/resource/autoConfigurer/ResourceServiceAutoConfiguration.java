package com.novel.resource.autoConfigurer;

import com.aliyun.oss.OSS;
import com.novel.common.resource.IResourceService;
import com.novel.framework.config.ResourceConfig;
import com.novel.resource.cos.autoConfigurer.CosClientFactoryBean;
import com.novel.resource.cos.config.CosConfig;
import com.novel.resource.cos.impl.CosConditional;
import com.novel.resource.cos.impl.CosResourceServiceImpl;
import com.novel.resource.cos.impl.MinioConditional;
import com.novel.resource.cos.impl.MinioResourceServiceImpl;
import com.novel.resource.minio.config.MinioConfig;
import com.novel.resource.oss.autoConfigurer.OssClientFactoryBean;
import com.novel.resource.oss.config.OssConfig;
import com.novel.resource.oss.impl.OssConditional;
import com.novel.resource.oss.impl.OssResourceServiceImpl;
import com.novel.resource.qiniu.QiniuClient;
import com.novel.resource.qiniu.impl.QiniuConditional;
import com.novel.resource.qiniu.impl.QiniuResourceServiceImpl;
import com.qcloud.cos.COSClient;
import io.minio.MinioClient;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;

/**
 * 资源服务自动注入
 *
 * @author novel
 * @date 2019/6/4
 */
@Configuration
@EnableConfigurationProperties(ResourceConfig.class)
@Slf4j
public class ResourceServiceAutoConfiguration {


    @Configuration
    @ConditionalOnClass({OSS.class, OssClientFactoryBean.class})
    @ConditionalOnProperty(prefix = ResourceConfig.RESOURCE_PREFIX, name = "enable", havingValue = "true")
    @AutoConfigureAfter({OssClientFactoryBean.class})
    @AllArgsConstructor
    static class OssResourceConfiguration {
        private ResourceConfig resourceConfig;

        @Conditional(OssConditional.class)
        @Bean
        @ConditionalOnMissingBean
        public IResourceService iResourceServiceFactory(OSS oss, OssConfig ossConfig) {
            log.info("OssResourceServiceImpl 初始化...");
            return new OssResourceServiceImpl(oss, ossConfig, this.resourceConfig.getFileCacheMaxTime());
        }
    }

    @Configuration
    @ConditionalOnClass({COSClient.class, CosClientFactoryBean.class})
    @ConditionalOnProperty(prefix = ResourceConfig.RESOURCE_PREFIX, name = "enable", havingValue = "true")
    @AutoConfigureAfter({CosClientFactoryBean.class})
    @AllArgsConstructor
    static class CosResourceConfiguration {
        private ResourceConfig resourceConfig;

        @Conditional(CosConditional.class)
        @Bean
        @ConditionalOnMissingBean
        public IResourceService iResourceServiceFactory(COSClient cosClient, CosConfig cosConfig) {
            log.info("CosResourceServiceImpl 初始化...");
            return new CosResourceServiceImpl(cosClient, cosConfig, resourceConfig.getFileCacheMaxTime());
        }
    }

    @Configuration
    @ConditionalOnClass({MinioClient.class})
    @ConditionalOnProperty(prefix = ResourceConfig.RESOURCE_PREFIX, name = "enable", havingValue = "true")
    @AllArgsConstructor
    static class MinioResourceConfiguration {
        private ResourceConfig resourceConfig;

        @Conditional(MinioConditional.class)
        @Bean
        @ConditionalOnMissingBean
        public IResourceService iResourceServiceFactory(MinioClient minioClient, MinioConfig minioConfig) {
            log.info("MinioResourceConfiguration 初始化...");
            return new MinioResourceServiceImpl(minioClient, minioConfig, resourceConfig.getFileCacheMaxTime());
        }
    }

    @Configuration
    @ConditionalOnClass({QiniuClient.class})
    @ConditionalOnProperty(prefix = ResourceConfig.RESOURCE_PREFIX, name = "enable", havingValue = "true")
    @AllArgsConstructor
    static class QiniuResourceConfiguration {
        private ResourceConfig resourceConfig;

        @Conditional(QiniuConditional.class)
        @Bean
        @ConditionalOnMissingBean
        public IResourceService iResourceServiceFactory(QiniuClient qiniuClient) {
            log.info("QiniuResourceServiceImpl 初始化...");
            return new QiniuResourceServiceImpl(qiniuClient, resourceConfig.getFileCacheMaxTime());
        }
    }


}
