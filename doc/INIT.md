#### [介绍文档](../README.md) | [部署文档](./BUILD.md) | 初始化文档

> 这里以本人的idea  2019.3.1 Ultimate 版为例，如果不是这个版本的，可以自行就行度娘，操作都大同小异，应该可以自主完成。
> 使用其他工具的，请自行百度一下吧~~~


### 下载项目 
打开项目`git`地址[Novel-api](https://gitee.com/cnovel/Novel-api)

找到`克隆/下载`，根据自己的方式，选择下载方式，本人这里通过`https`方式进行。
![下载](https://oscimg.oschina.net/oscnet/up-c97835bd72a5c499a41644e357ba5130f49.JPEG "下载")
点击复制，复制项目的`git`地址。


###  导入项目
打开`idea`，如果你没有打开项目，项目下图一样，则根据图示，选择即可：
![导入项目](https://oscimg.oschina.net/oscnet/up-c73e4ab16c4f7aaae7994a265eb4d319eda.JPEG "导入项目")

如果你已经打开项目，则根据以下图进行选择：
![导入](https://oscimg.oschina.net/oscnet/up-4076fa0171982ac6f880e6670d920871264.JPEG "导入")
![导入](https://oscimg.oschina.net/oscnet/up-9dd639c023a6c6fbfad324012f42dedb9a6.JPEG "导入")

然后在`url`地址栏中填写刚刚复制的项目地址即可。

###  配置项目

如果刚导入的项目已经如下图自动配置完成了，则无需再去手动配置，如果没有，则跟着作者进行检查哪些没有配置好，需要手动配置的。
![模块配置正确图](https://oscimg.oschina.net/oscnet/up-d702213a315c290ef02fd0aec47da778b96.JPEG "模块配置正确图")

* 如果`java`模块没有正确配置，则根据下图进行手动配置：

打开设置
![打开设置](https://oscimg.oschina.net/oscnet/up-7ec6cf4b186c4f43233c581485665f4c6ec.JPEG "打开设置")

选择模块信息
![选择模块信息](https://oscimg.oschina.net/oscnet/up-cc0f32e71d76cedef154ae442483b0b16e7.JPEG "选择模块信息")

手动选择添加想要的模块
![添加模块](https://oscimg.oschina.net/oscnet/up-63c7f41d7a3b24aa3c19f2cf785c7cd8bae.JPEG "添加模块")

到此，`java`模块配置完成。

* 如果`maven`模块没有正确配置，则根据下图进行手动添加配置：

找到没有添加的`maven`模块，找到`pom.xml`文件，此时的`pom.xml`应该是橘黄色的，右击`pom.xmnl`文件，点击`add maven project`，配置完成。

![添加maven模块](https://oscimg.oschina.net/oscnet/up-9a75685a23d562b5eeba8a7e766e8457e16.JPEG "添加maven模块")

* 如果系统安装了多个`java`版本，切换`java`版本，如下图（`项目默认编译环境为jdk1.8`）：

打开项目的设置：
![打开设置](https://oscimg.oschina.net/oscnet/up-7ec6cf4b186c4f43233c581485665f4c6ec.JPEG "打开设置")

找到project
![java设置](https://oscimg.oschina.net/oscnet/up-29fdb93cc28998a8ba24b7040313831295d.JPEG "java设置")

![java设置](https://oscimg.oschina.net/oscnet/up-d54ecc992096706f60178c9a37abaf82b2c.JPEG "java设置")

此时应该项目模块信息正常了，如果还有其他问题，请自行度娘。。。

### 配置项目属性文件
如果首次下载项目，那么项目的配置文件会是黄色的警告色，则需要编译项目。

![属性文件警告](https://oscimg.oschina.net/oscnet/up-0c7a8bf2bf03b26299291d1ad6a283d92f3.JPEG "属性文件警告")

通过maven对项目进行编译：

![编译项目](https://oscimg.oschina.net/oscnet/up-4469ae164b09091d7a01c92f2b94cc1947c.JPEG "编译项目")

![编译信息](https://oscimg.oschina.net/oscnet/up-6f8c3948c1715a0a717b51d73fd499d1ec9.JPEG "编译信息")

![编译信息](https://oscimg.oschina.net/oscnet/up-0bbbe1e90085f11ad66708881c20f7d660b.JPEG "编译信息")

编译完成后，属性文件的配置信息变成正常颜色，并且配置时会有相应的配置项提示。

具体配置信息请参考[部署文档](./BUILD.md)

### 运行项目

基本属性配置完成后，则就可以运行项目了，找到admin模块下的`NovelApplication.java`，右击运行。

![运行项目](https://oscimg.oschina.net/oscnet/up-5c0db1090dab67eb501bbb83920ef47c2a8.JPEG "运行项目")


### Q&A

Q: 出现`Error creating bean with name 'cosClientFactoryBean' defined in class path resource`错误的。

A：如果你看过文档，对整个进行了package或者install，应该不会出现这个问题，建议install一下，避免clean时出现错误。

Q:出现

```
ERROR org.springframework.boot.SpringApplication - Application run failed org.springframework.beans.factory.BeanCreationException: Error creating bean with name 'cosClientFactoryBean' defined in class path resource [com/novel/resource/cos/autoConfigurer/CosAutoConfiguration.class]: Invocation of init method failed; nested exception is java.lang.IllegalArgumentException: Access key cannot be null
```

A:这是因为项目默认使用了`cos`，而你没有配置`cos`信息导致的，下次我直接改成默认不用对象存储吧。 你在配置信息里加上
```yaml
cos:
  bucket-name: 
  region: 
  secret-id: 
  secret-key: 
```

或者删除`admin`模块里面的

```xml
<dependency>
    <groupId>com.novel</groupId>
    <artifactId>cos-spring-boot-starter</artifactId>
</dependency>  
<dependency>
    <groupId>com.novel</groupId>
    <artifactId>resource-spring-boot-starter</artifactId>
</dependency>
```
 
依赖，并且设置配置项
```yaml
resource:
  enable: false
```

删除依赖时建议clean一下，并且刷新一下`maven`依赖，确保`maven`不存在缓存。














