package com.novel.framework.enums;

/**
 * 操作人类别
 *
 * @author novel
 * @date 2019/5/14
 */
public enum OperatorType {
    /**
     * 其它
     */
    OTHER,

    /**
     * 后台用户
     */
    MANAGE,

    /**
     * 手机端用户
     */
    MOBILE
}
