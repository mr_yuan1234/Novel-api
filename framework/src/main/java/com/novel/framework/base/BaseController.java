package com.novel.framework.base;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.novel.common.utils.DateUtils;
import com.novel.common.utils.StringUtils;
import com.novel.common.utils.sql.SqlUtil;
import com.novel.framework.result.Result;
import com.novel.framework.shiro.utils.ShiroUtils;
import com.novel.framework.web.page.PageDomain;
import com.novel.framework.web.page.TableDataInfo;
import com.novel.framework.web.page.TableSupport;
import com.novel.system.domain.SysUser;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import java.beans.PropertyEditorSupport;
import java.util.Date;
import java.util.List;

/**
 * web层通用数据处理
 *
 * @author novel
 * @date 2019/4/17
 */
public class BaseController {
    /**
     * 将前台传递过来的日期格式的字符串，自动转化为Date类型
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        // Date 类型转换
        binder.registerCustomEditor(Date.class, new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) {
                setValue(DateUtils.parseDate(text));
            }
        });
    }

    /**
     * 设置请求分页数据
     */
    protected void startPage() {
        PageDomain pageDomain = TableSupport.buildPageRequest();
        Integer pageNum = pageDomain.getPageNum();
        Integer pageSize = pageDomain.getPageSize();
        if (StringUtils.isNotNull(pageNum) && StringUtils.isNotNull(pageSize) && StringUtils.isNotEmpty(pageDomain.getOrderBy())) {
            String orderBy = SqlUtil.escapeOrderBySql(pageDomain.getOrderBy());
            PageHelper.startPage(pageNum, pageSize, orderBy);
        } else if (StringUtils.isNotEmpty(pageDomain.getOrderBy())) {
            String orderBy = SqlUtil.escapeOrderBySql(pageDomain.getOrderBy());
            PageHelper.orderBy(orderBy);
        } else if (StringUtils.isNotNull(pageNum) && StringUtils.isNotNull(pageSize)) {
            PageHelper.startPage(pageNum, pageSize);
        }
    }

    /**
     * 响应请求分页数据
     */
    protected <E> TableDataInfo getDataTable(List<E> list) {
        TableDataInfo rspData = new TableDataInfo();
        rspData.setCode(200);
        rspData.setRows(list);
        if (list == null) {
            rspData.setTotal(0);
        } else {
            rspData.setTotal(new PageInfo<>(list).getTotal());
        }
        return rspData;
    }

    /**
     * 设置用户
     *
     * @param user 用户信息
     */
    protected void setUser(SysUser user) {
        ShiroUtils.setUser(user);
    }

    /**
     * 响应返回结果
     *
     * @param result 影响结果
     * @return 操作结果
     */
    protected Result toAjax(boolean result) {
        return result ? success() : error();
    }

    /**
     * 响应返回结果
     *
     * @param data 数据
     * @return 操作结果
     */
    protected Result toAjax(Object data) {
        return Result.success(data);
    }

    /**
     * 响应返回结果
     *
     * @param result  影响结果
     * @param success 成功消息
     * @return 操作结果
     */
    protected Result toAjax(boolean result, String success) {
        return result ? success(success) : error();
    }

    /**
     * 响应返回结果
     *
     * @param result  影响结果
     * @param success 成功消息
     * @param error   失败结果
     * @return 操作结果
     */
    protected Result toAjax(boolean result, String success, String error) {
        return result ? success(success) : error(error);
    }

    /**
     * 返回成功
     */
    protected Result success() {
        return Result.success();
    }

    /**
     * 返回失败消息
     */
    protected Result error() {
        return Result.error();
    }

    /**
     * 返回成功消息
     */
    protected Result success(String message) {
        return Result.success(message);
    }

    /**
     * 返回失败消息
     */
    protected Result error(String message) {
        return Result.error(message);
    }

    /**
     * 返回错误码消息
     */
    protected Result error(int code, String message) {
        return Result.error(message, code);
    }

    /**
     * 获取当前用户
     *
     * @return 用户
     */
    protected SysUser getUser() {
        return ShiroUtils.getUser();
    }

    /**
     * 获取当前用户id
     *
     * @return 用户id
     */
    protected Long getUserId() {
        return getUser().getId();
    }

    /**
     * 获取当前用户用户名
     *
     * @return 用户名
     */
    protected String getUserName() {
        return getUser().getUserName();
    }
}
