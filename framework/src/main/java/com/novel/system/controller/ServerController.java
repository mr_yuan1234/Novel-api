package com.novel.system.controller;

import com.novel.framework.base.BaseController;
import com.novel.framework.result.Result;
import com.novel.framework.web.domain.Server;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 服务器监控
 *
 * @author novel
 * @date 2019/5/21
 */
@RestController
@RequestMapping("/monitor/server")
public class ServerController extends BaseController {
    /**
     * 获取服务器运行信息
     *
     * @return 服务器运行信息
     */
    @RequiresPermissions("monitor:server:list")
    @GetMapping()
    public Result server() {
        Server server = new Server();
        server.copyTo();
        return toAjax(server);
    }
}
