package com.novel.system.domain;

import com.novel.framework.base.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 角色和菜单关联 sys_role_menu
 *
 * @author novel
 * @date 2019/4/16
 */
@Data
@ToString
@EqualsAndHashCode(callSuper = false)
public class SysRoleMenu extends BaseModel {
    private static final long serialVersionUID = 1L;
    /**
     * 角色ID
     */
    private Long roleId;

    /**
     * 菜单ID
     */
    private Long menuId;
}
