package com.novel.system.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.novel.common.utils.StringUtils;
import com.novel.system.domain.LoginUser;
import com.novel.system.domain.SysDept;
import com.novel.system.domain.SysUserOnline;
import com.novel.system.service.SysDeptService;
import com.novel.system.service.SysUserOnlineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * 在线用户 服务层处理
 *
 * @author novel
 * @date 2019/12/13
 */
@Service
public class SysUserOnlineServiceImpl implements SysUserOnlineService {
    private final SysDeptService sysDeptService;

    public SysUserOnlineServiceImpl(SysDeptService sysDeptService) {
        this.sysDeptService = sysDeptService;
    }

    /**
     * 设置在线用户信息
     *
     * @param user 用户信息
     * @return 在线用户
     */
    @Override
    public SysUserOnline loginUserToUserOnline(LoginUser user) {
        if (ObjectUtil.isNull(user) && ObjectUtil.isNull(user.getUser())) {
            return null;
        }
        SysUserOnline sysUserOnline = new SysUserOnline();
        sysUserOnline.setIpaddr(user.getIpaddr());
        sysUserOnline.setLoginName(user.getUser().getUserName());
        sysUserOnline.setSessionId(user.getSessionId());
        sysUserOnline.setBrowser(user.getBrowser());
        sysUserOnline.setLoginLocation(user.getLoginLocation());
        sysUserOnline.setOs(user.getOs());
        sysUserOnline.setLastAccessTime(new Date(user.getLoginTime()));
        sysUserOnline.setStartTimestamp(new Date(user.getLoginTime()));
        sysUserOnline.setExpireTime(user.getExpireTime());
        SysDept dept = sysDeptService.selectDeptById(user.getUser().getDeptId());
        sysUserOnline.setDeptName(dept.getDeptName());
        return sysUserOnline;
    }

    /**
     * 通过登录地址/用户名称查询信息
     *
     * @param ipaddr   登录地址
     * @param userName 用户名称
     * @param user     用户信息
     * @return 在线用户信息
     */
    @Override
    public SysUserOnline selectOnlineByInfo(String ipaddr, String userName, LoginUser user) {
        if (StringUtils.equals(ipaddr, user.getIpaddr()) && StringUtils.equals(userName, user.getUser().getUserName())) {
            return loginUserToUserOnline(user);
        }
        return null;
    }

    /**
     * 通过用户名称查询信息
     *
     * @param userName 用户名称
     * @param user     用户信息
     * @return 在线用户信息
     */
    @Override
    public SysUserOnline selectOnlineByUserName(String userName, LoginUser user) {
        if (StringUtils.equals(userName, user.getUser().getUserName())) {
            return loginUserToUserOnline(user);
        }
        return null;
    }

    /**
     * 通过登录地址查询信息
     *
     * @param ipaddr 登录地址
     * @param user   用户信息
     * @return 在线用户信息
     */
    @Override
    public SysUserOnline selectOnlineByIpaddr(String ipaddr, LoginUser user) {
        if (StringUtils.equals(ipaddr, user.getIpaddr())) {
            return loginUserToUserOnline(user);
        }
        return null;
    }
}
