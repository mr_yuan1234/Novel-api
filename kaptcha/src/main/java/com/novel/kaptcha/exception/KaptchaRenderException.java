package com.novel.kaptcha.exception;

/**
 * 生成验证码异常
 *
 * @author novel
 * @date 2019/9/28
 */
public class KaptchaRenderException extends KaptchaException {

    public KaptchaRenderException(Exception e) {
        super(e);
    }

    public KaptchaRenderException(String message) {
        super(message);
    }

    public KaptchaRenderException() {
    }
}
